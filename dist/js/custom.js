// JavaScript Document
/*********************/
// Location Search Filter
//<![CDATA[
function myLocate() {
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("locatInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
//]]>
//end ocation Search Filter


jQuery(document).ready(function() {    
    //Sticky Menu
    jQuery('#nav_bg').stickit({scope: StickScope.Document, zIndex: 101}); 
    jQuery('.detail_head').stickit({scope: StickScope.Document, zIndex: 101}); 
    jQuery('.reviewTab_menu').stickit({scope: StickScope.Document, zIndex: 101});  
    
    /// OTP Field
    $('#pincode_input').pincodeInput({
        hidedigits:true,
        inputs:4,
        placeholders:". . . .",
    });

    /// Flish Page slider
    var owl = jQuery('#flash_slider');
    owl.owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: true,
        nav: false,       
    }) 
    /// Search Filter Slider
    var owl = jQuery('#trendList_food');
    owl.owlCarousel({
        stagePadding: 20,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: false,
        nav: false,
        responsive:{
            0: {
                items: 3
            },
            480: {
                items: 3
            },
            600: {
                items: 5
            },
            768: {
                items: 7
            },
            1024: {
                items: 9 
            }
        }
    }) 
    /// Search Checkbox Slider
    var owl = jQuery('#checkFilter_food');
    owl.owlCarousel({
        stagePadding: 20,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: false,
        nav: false,
        responsive:{
            0: {
                items: 3
            },
            480: {
                items: 3
            },
            600: {
                items: 5
            },
            768: {
                items: 7
            },
            1024: {
                items: 9 
            }
        }
    }) 
    /// Home page Banner slider
    var owl = jQuery('#srcResult_slider');
    owl.owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: true,
        nav: false,       
    }) 
    /// Home page Banner slider
    var owl = jQuery('#banner_food');
    owl.owlCarousel({
        stagePadding: 20,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: false,
        nav: false,
        //navText : ["&#xe875","&#xe876"],
        responsive:{
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            600: {
                items: 1
            },
            768: {
                items: 1
            },
            1024: {
                items: 1 
            }
        }
    }) 
    /// Legendry Food slider
    var owl = jQuery('#legendry_food');
    owl.owlCarousel({
        stagePadding: 20,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: false,
        nav: false,
        //navText : ["&#xe875","&#xe876"],
        responsive:{
            0: {
                items: 2
            },
            480: {
                items: 2
            },
            600: {
                items: 2
            },
            768: {
                items: 4
            },
            1024: {
                items: 5 
            }
        }
    }) 
    /// Trending this week slider
    var owl = jQuery('#trendWeek_food');
    owl.owlCarousel({
        stagePadding: 20,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: false,
        nav: false,
        responsive:{
            0: {
                items: 2
            },
            480: {
                items: 2
            },
            600: {
                items: 2
            },
            768: {
                items: 4
            },
            1024: {
                items: 5 
            }
        }
    }) 
    /// Collections by Capi slider  fevCuisine_slide
    var owl = jQuery('#collect_capi');
    owl.owlCarousel({
        stagePadding: 20,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: false,
        nav: false,
        responsive:{
            0: {
                items: 2
            },
            480: {
                items: 2
            },
            600: {
                items: 2
            },
            768: {
                items: 4
            },
            1024: {
                items: 5 
            }
        }
    }) 
    /// Favorite Cuisines slider  
    var owl = jQuery('#fevCuisine_slide');
    owl.owlCarousel({
        stagePadding: 20,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: false,
        nav: false,
        responsive:{
            0: {
                items: 2
            },
            480: {
                items: 2
            },
            600: {
                items: 2
            },
            768: {
                items: 4
            },
            1024: {
                items: 5 
            }
        }
    }) 
    /// Popular Brands slider  
    var owl = jQuery('#popBrnd_slide');
    owl.owlCarousel({
        stagePadding: 20,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause:true,
        navRewind: false,
        dots: false,
        nav: false,
        responsive:{
            0: {
                items: 2
            },
            480: {
                items: 2
            },
            600: {
                items: 2
            },
            768: {
                items: 4
            },
            1024: {
                items: 5 
            }
        }
    }) 
    // end Owl

    jQuery(".modal").on("show", function () {
        jQuery("body").addClass("modal-open");
    }).on("hidden", function () {
        jQuery("body").removeClass("modal-open")
    });
})

jQuery(document).on("click","#lightBox_btn",function() {
    var image=jQuery(this).attr('data-image');
    jQuery('#img_modal').modal('toggle');
    jQuery("#imgShow_src").attr("src",image);
});








	
